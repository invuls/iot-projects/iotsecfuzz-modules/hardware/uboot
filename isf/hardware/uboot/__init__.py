from isf.core import logger
import isf.hardware.ttltalker
import time
from .config import prefixes
import re
import os


class UBoot:
    baudrate = 9600
    com_port = '/dev/ttyACM0'
    uart_obj = None
    timeout = 0.1
    newline_bytes = '0d0a'
    turn_off_time = 5
    init_console_bytes = '03'
    console_status = 0
    current_prefix = 'NOTEXIST'
    cmd_list_array = []

    def __init__(self, com_port='/dev/ttyUSB0', baudrate=115200, timeout=0.01,
                 newline_bytes="0d0a", turn_on_time=10,
                 init_console_bytes="03", init_console=True, cmd_prefix="00"):
        self.baudrate = baudrate
        self.com_port = com_port
        self.timeout = timeout
        self.newline_bytes = bytes.fromhex(newline_bytes).decode('charmap')
        self.turn_on_time = turn_on_time
        self.init_console_bytes = init_console_bytes
        self.newline_bytes_hex = newline_bytes
        self.console_status = not init_console
        if cmd_prefix == '00':
            self.cmd_prefix = prefixes
        else:
            self.cmd_prefix = [bytes.fromhex(cmd_prefix).decode('charmap')]
        self.create_connection()

    def create_connection(self):
        self.uart_obj = isf.hardware.ttltalker.TTLTalker(self.com_port,
                                                         self.baudrate,
                                                         self.timeout)
        if not self.uart_obj:
            logger.error("Can't connect to UBoot console!")
            return -1

    def init_console(self):
        if not self.uart_obj:
            result = self.create_connection()
            if result == -1:
                return -1
        logger.info(
            "Please, turn off IoT device and wait for {} seconds!".format(
                self.turn_off_time))
        time.sleep(self.turn_off_time)
        start_time = time.time()
        print_once = 0
        while time.time() - start_time < self.turn_on_time + 5:  # 5 - turn on human time
            if not print_once:
                logger.info("Turn on IoT device.. NOW!")
                print_once = 1
            self.uart_obj.send_bytes(self.init_console_bytes, True,
                                     100, '')
        self.console_status = self.detect_prefix()

    def detect_prefix(self):
        logger.info("Checking for initiation of console")
        clr = self.clear_console().decode('charmap')
        ans = self.uart_obj.send_bytes(self.newline_bytes_hex, True, 10000, '')
        if 'ret_str' in ans:
            logger.debug('Returned: {}'.format(ans['ret_str']))
        else:
            logger.debug('Nothing returned, may be errors!')
            return 0

        full_result = clr + ans['ret_str'].decode('charmap')
        for prefix in prefixes:
            if prefix in full_result:
                self.current_prefix = prefix
                return 1
        logger.debug(full_result)
        logger.error(
            "Can't find U-Boot CLI prefix. If it is not default, change it in config.py of uboot model.")
        exit()
        return 0

    def clear_console(self):
        # only reading from console
        ans = b''
        ans += self.uart_obj.send_bytes('', True, 10000, '')['ret_str']
        ans += self.uart_obj.send_bytes('', True, 10000, '')['ret_str']
        ans += self.uart_obj.send_bytes('', True, 10000, '')['ret_str']
        return ans

    def send_cmd(self, cmd="help"):
        if not self.console_status and self.console_status == 0:
            res = self.init_console()
            if res == -1:
                return -1
        elif self.current_prefix == 'NOTEXIST':
            self.detect_prefix()
        self.clear_console()
        logger.debug("Command: {}".format(cmd))
        logger.debug("HEX command: {}".format(cmd.encode("charmap").hex()))
        ans = self.uart_obj.send_bytes(cmd.encode("charmap").hex(), output=True,
                                       output_length=10000,
                                       newline_bytes=self.newline_bytes_hex)[
            'ret_str']
        full_result = ans.decode('charmap')
        while not full_result.endswith(self.current_prefix):
            ans = \
                self.uart_obj.send_bytes('',
                                         output=True,
                                         output_length=10000,
                                         newline_bytes='')['ret_str']
            rest_ans = ans.decode('charmap')
            full_result += rest_ans
            logger.debug(full_result)
        logger.debug('RAW cmd answer:')
        logger.debug(full_result)
        while full_result.startswith(cmd + self.newline_bytes):
            full_result = full_result[len(cmd + self.newline_bytes):]
        while full_result.endswith(self.newline_bytes + self.current_prefix):
            full_result = full_result[
                          :-len(self.newline_bytes + self.current_prefix)]
        return {'result': full_result}

    def cmd_list(self):
        help_cmd = "help"
        if self.cmd_list_array:
            return {'commands': self.cmd_list_array}
        ans = self.send_cmd(help_cmd)['result']
        # TODO: add regexp for parsing
        arr = ans.split(self.newline_bytes)
        result = {}
        for line in arr:
            cmd_and_descr = line.split('-')
            result[cmd_and_descr[0].strip()] = '-'.join(
                cmd_and_descr[1:]).strip()
        self.cmd_list_array = result
        return {'commands': result}

    def get_envs(self):
        env_cmd = "printenv"
        ans = self.send_cmd(env_cmd)['result']
        # TODO: add regexp for parsing
        arr = ans.split(self.newline_bytes)
        result = {}
        env_size_bytes = 0
        full_size_bytes = 0
        for line in arr:
            cmd_and_descr = line.split('=')
            if cmd_and_descr[0].strip().startswith('Environment size: '):
                str_arr = cmd_and_descr[0].strip().split('/')
                env_size_bytes = int(str_arr[0].split(' ')[-1])
                full_size_bytes = int(str_arr[1].split(' ')[0])
            if cmd_and_descr[0].strip() != '' and not cmd_and_descr[
                0].strip().startswith('Environment size: '):
                result[cmd_and_descr[0].strip()] = '='.join(
                    cmd_and_descr[1:]).strip()
        return {'environments': result, 'env_size': env_size_bytes,
                'full_size': full_size_bytes}

    def get_version(self):
        help_cmd = "version"
        ans = self.send_cmd(help_cmd)['result'].strip(' \t\r\n')
        return {'version': ans}

    def prepare_to_boot(self):
        boot_env = "bootcmd"
        boot_delay = 10
        boot_command = self.get_envs()["environments"][boot_env]
        boot_changed = ';'.join(boot_command.split(';')[0:-1])
        # TODO: fix for getting address
        logger.info("Wait for loading firmware! (10 sec max)")
        tmp_timeout = self.uart_obj.serial_link.timeout
        self.uart_obj.serial_link.timeout = boot_delay
        ans = self.send_cmd(boot_changed)['result']
        logger.info(
            "If you need to get output (may be with load address), run this module with debugging mode")
        logger.debug(ans)
        self.uart_obj.serial_link.timeout = tmp_timeout
        logger.info("Done! Check it!")

    def memory_dumper(self, dump_type="serial", savepath="/tmp/",
                      offset="0x40000", length="0x150"):
        read_cmd = 'md.l {}'
        r_get_values = "([0-9a-fA-F]{8}): ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8})    (.{16})"
        orig_offset = offset
        # if hex number
        if not isinstance(offset, int):
            orig_offset = int(offset, 16)
        curr_offset = (orig_offset // 0x100) * 0x100
        # if hex number
        if not isinstance(length, int):
            length = int(length, 16)
        memory_bytes = b''
        logger.info('Starting to dump memory!')
        if dump_type == 'serial':
            while curr_offset - orig_offset < length + (orig_offset % 0x100):
                logger.debug(
                    "Reading memory from {} to {}".format(hex(curr_offset),
                                                          hex(
                                                              curr_offset + 0x100)))
                runcmd = read_cmd.format(hex(curr_offset))
                ans = self.send_cmd(runcmd)['result']
                logger.debug('Command: ' + runcmd)
                logger.debug(ans)
                for line in ans.split(self.newline_bytes):
                    if bool(re.match(r_get_values, line)):
                        res = re.match(r_get_values, line)
                        memory_bytes += b''.join(
                            [bytes.fromhex(x)[::-1] for x in res.groups()[1:5]])
                    else:
                        logger.error(
                            'Found exception at device while dumping memory')
                        logger.debug('Doesn\'t match the regexp:')
                        logger.debug(ans)
                        return
                curr_offset += 0x100
                memory_bytes = memory_bytes[orig_offset % 0x100: length + (
                        orig_offset % 0x100)]
            logger.info(
                'Saving downloaded memory of size {} bytes to {}'.format(
                    len(memory_bytes), savepath))
            f = open(savepath, 'wb')
            f.write(memory_bytes)
            f.close()
            return {'bin_size': len(memory_bytes)}
        if dump_type == 'microsd':
            # TODO: add method
            pass
        if dump_type == 'tftp':
            # TODO: add method
            # TODO: also add functions for any type of dumping
            pass

    def firmware_dumper(self, dump_type="serial", savepath="/tmp/",
                        offset="0x40000", length="0x150"):
        # preparing for dumping
        self.prepare_to_boot()
        self.memory_dumper(dump_type, savepath, offset, length)

    def get_shell(self):
        pwn_commands = [
            "setenv extra_boot_args init=/bin/sh",
            "setenv optargs init=/bin/sh",
            "setenv bootargs ${bootargs} single init=/bin/sh"
        ]
        boot_cmd = "boot"
        for command in pwn_commands:
            self.send_cmd(command)
        new_envs = self.get_envs()
        logger.debug(new_envs)
        self.send_cmd(boot_cmd)

        logger.info(
            "Preparing for BOOT! Try to connect to console to get shell!")

    def interactive_console(self):
        logger.info('Starting console. Print "pwnexit" to exit from console')
        while 1:
            cmd = input('Command: ')
            if cmd != 'pwnexit':
                result = self.send_cmd(cmd)['result']
                logger.info('=========RESULT=========')
                logger.info(result)
                logger.info('==========END==========')
            else:
                logger.info('Exiting!')
                return

    def cramfs_ls(self, path_ls="/", not_found_str="corresponding entry", need_to_prepare=True):
        cramfsls_cmd = 'cramfsls'
        cramfsls_changed = cramfsls_cmd + ' ' + path_ls
        reg_ls = ' ([^ ]+)[ ]+([\d]*) (.+)'
        cmd_list = self.cmd_list()['commands']
        logger.debug(str(cmd_list))
        if not cramfsls_cmd in cmd_list:
            logger.error(
                'Command "{}" not found! Closing...'.format(cramfsls_cmd))
            return -1
        if need_to_prepare:
            self.prepare_to_boot()
        ans = self.send_cmd(cramfsls_changed)['result']
        if not_found_str in ans:
            logger.error('Path {} not found!'.format(path_ls))
            return -1
        dir_arr = ans.split(self.newline_bytes)
        l = []
        for line in dir_arr:
            if bool(re.match(reg_ls, line)):
                res = re.match(reg_ls, line)
                obj = {}
                arr = res.groups()
                logger.debug(str(arr))
                obj['size'] = int(arr[1])
                if arr[0].startswith('d'):
                    obj['type'] = 'dir'
                elif arr[0].startswith('-'):
                    obj['type'] = 'file'
                elif arr[0].startswith('l'):
                    obj['type'] = 'symlink'
                else:
                    obj['type'] = 'other,' + arr[0][0]
                obj['name'] = arr[2]
                if obj['type'] == 'symlink':
                    obj['original'] = obj['name'].split(' -> ')[1]
                    obj['name'] = obj['name'].split(' -> ')[0]
                obj['priv'] = arr[0]
                l.append(obj)
        return {'filelist': l}

    def cramfs_load(self, path_load="/etc/passwd", not_found_str='corresponding entry', path_to_save="1.bin",
                    need_to_prepare=True,
                    dump_type="serial", readfile_timeout=2, success_load_string="CRAMFS load complete:",
                    checkcmd=True):
        cramfsload_cmd = 'cramfsload'
        cramfsload_run_cmd = cramfsload_cmd + ' ' + path_load

        logger.info('CramFSload: {}'.format(path_load))
        cmd_list = self.cmd_list()['commands']
        if not cramfsload_cmd in cmd_list:
            logger.error(
                'Command "{}" not found! Closing...'.format(cramfsload_cmd))
            return -1
        if need_to_prepare:
            self.prepare_to_boot()
        tmp_timeout = self.uart_obj.serial_link.timeout
        self.uart_obj.serial_link.timeout = readfile_timeout
        ans = self.send_cmd(cramfsload_run_cmd)['result']
        self.uart_obj.serial_link.timeout = tmp_timeout
        if not_found_str in ans:
            logger.info('Path was not found!')
            return -1
        elif not success_load_string in ans:
            logger.info('Memory load error!')
            return -1
        logger.debug(ans)
        # TODO: add regexp
        # TODO: also add regexps for other changed U-Boot loaders

        filesize_bytes = int(ans.split(': ')[1].split(' ')[0])
        file_address = int(ans.split(' ')[-1], 16)
        logger.info("Filesize: '{}', fileaddress: '{}'".format(filesize_bytes,
                                                               file_address))
        res = self.memory_dumper(dump_type, path_to_save, file_address,
                                 filesize_bytes)
        logger.info('File {} was loaded with size {} bytes.'.format(path_load,
                                                                    filesize_bytes))
        return res

    def cramfs_dump(self, dump_folder="/", save_dir="./firmware", not_found_str="corresponding entry", need_to_prepare=True,
                    dump_type="serial", readfile_timeout=2, success_load_string="CRAMFS load complete:"):

        # creating folder
        os.makedirs(os.path.dirname(save_dir), exist_ok=True)
        if need_to_prepare:
            self.prepare_to_boot()

        filelist = self.cramfs_dump_from_folder(dump_folder, not_found_str,
                                                save_dir, dump_folder,
                                                dump_folder,
                                                dump_type,
                                                readfile_timeout,
                                                success_load_string, False)
        return {'filelist': filelist}

    def cramfs_dump_from_folder(self, path, not_found_str, syspath, folder,
                                root_dir, dump_type, readfile_timeout,
                                success_load_string,
                                need_to_prepare=False):
        '''recursive function'''
        filelist = self.cramfs_ls(path, not_found_str, need_to_prepare)[
            'filelist']
        logger.debug(str(filelist))
        time.sleep(10)
        downloaded = []
        for file in filelist:
            if file['type'] == 'file':
                new_dir_path = syspath.rstrip('/') + '/' + folder.lstrip(
                    root_dir).strip('/')
                load_path = folder.rstrip('/') + '/' + file['name'].lstrip('/')
                save_path = new_dir_path.rstrip('/') + '/' + file[
                    'name'].lstrip('/')
                os.makedirs(os.path.dirname(new_dir_path + '/'), exist_ok=True)
                downloaded.append(save_path)
                self.cramfs_load(load_path, not_found_str, save_path,
                                 need_to_prepare, dump_type, readfile_timeout,
                                 success_load_string)
            if file['type'] == 'dir':
                new_dir_path = folder.rstrip('/') + '/' + file['name'].lstrip(
                    '/')
                logger.debug('Recursive folder {}'.format(new_dir_path))
                downloaded += self.cramfs_dump_from_folder(new_dir_path,
                                                           not_found_str,
                                                           syspath,
                                                           new_dir_path,
                                                           root_dir, dump_type,
                                                           readfile_timeout,
                                                           success_load_string)
        return downloaded
